package com.example.datatranslate.controller;
/**
* @Project: controller
* @description: SignController.java
* @date: Tuesday, 3rd August 2021 10:13:58 am
* @author: crisen (crisen@crisen.org)
* @Copyright 2021 - 2021 forfree, idreamsky.inc
*/

import com.example.datatranslate.pojo.request.SignQuery;
import com.example.datatranslate.service.TaskService;

import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@RestController
@AllArgsConstructor
public class SignController {

    private final ApplicationContext applicationContext;


    @RequestMapping("/sign/run")
    @ResponseBody
    public String migration(@RequestBody SignQuery query) {
        ExecutorService service = Executors.newFixedThreadPool(5);
        TaskService task = (TaskService) applicationContext.getBean(TaskService.class);
        // 可自行添加参数
        // System.out.println(query);
        task.setQuery(query);
        service.execute(task);
        return "ok";
    }
}
