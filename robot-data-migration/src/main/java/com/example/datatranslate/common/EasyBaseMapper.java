package com.example.datatranslate.common;

import java.util.Collection;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface EasyBaseMapper<T> extends BaseMapper<T>{
    Integer insertBatchSomeColumn(Collection<T> entitis);
}

