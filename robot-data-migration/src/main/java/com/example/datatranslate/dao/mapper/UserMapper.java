package com.example.datatranslate.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.datatranslate.dao.User;

public interface UserMapper extends BaseMapper<User> {

}
