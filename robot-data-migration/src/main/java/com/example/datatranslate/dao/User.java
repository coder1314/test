package com.example.datatranslate.dao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Builder
@Data
@TableName("fbt_inf_users")
public class User {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer systemId;

    private String tenantId;

    private Integer appId;

    private String nickname;

    private Date createdAt;

    private Date updatedAt;

    private String password;
}
