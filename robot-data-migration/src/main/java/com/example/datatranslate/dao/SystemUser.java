package com.example.datatranslate.dao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@TableName("fbt_inf_system_users")
@Data
@Builder
@ToString
public class SystemUser {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String username;

    private String name;

    private String nickname;

    private String avatar;

    private Date createdAt;

    private Date updatedAt;
}
