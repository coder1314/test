package com.example.datatranslate.dao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@Builder
@ToString
@TableName("fbt_inf_socialites")
public class Socialite {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String socialId;

    private String socialType;

    private String name;

    private String nickname;

    private Integer systemId;

    private String avatar;

    private Date createdAt;

    private Date updatedAt;
}
