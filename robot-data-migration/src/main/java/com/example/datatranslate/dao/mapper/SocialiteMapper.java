package com.example.datatranslate.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.datatranslate.dao.Socialite;

public interface SocialiteMapper extends BaseMapper<Socialite> {
}
