package com.example.datatranslate.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.datatranslate.dao.Sign;

public interface SignMapper extends BaseMapper<Sign> {
}
