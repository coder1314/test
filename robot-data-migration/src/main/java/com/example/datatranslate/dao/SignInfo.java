package com.example.datatranslate.dao;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@TableName("fanbook_sign")
@Builder
public class SignInfo {
    private String uid;

    private Integer username;

    private String nickName;

    private String guildId;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date signDay;

    private Long created;
}
