package com.example.datatranslate.dao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
@TableName("fbt_inf_sign_infos")
public class Sign {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer appId;

    private Integer userId;

    private Integer historySignDays;

    private Integer signDays;

    private Date lastSignDate;

    private Integer maxContinuingDays;

    private Integer continuingDays;

    private Date createdAt;

    private Date updatedAt;
}
