package com.example.datatranslate.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.datatranslate.dao.Record;

public interface RecordMapper extends BaseMapper<Record> {
}
