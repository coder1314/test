package com.example.datatranslate.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.datatranslate.dao.SignInfo;

public interface SignInfoMapper extends BaseMapper<SignInfo> {

}
