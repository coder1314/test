package com.example.datatranslate.dao.mapper;

// import java.util.Collection;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.datatranslate.dao.SystemUser;

public interface SystemUserMapper extends BaseMapper<SystemUser> {
    // <T> Integer insertBatchColumn(Collection<T> entities);
}
