package com.example.datatranslate.dao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
@TableName("fbt_inf_sign_records")
public class Record {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer appId;

    private Integer systemId;

    private Integer userId;

    private Date signDate;

    private Date signAt;

    private Date createdAt;

    private Date updatedAt;
}
