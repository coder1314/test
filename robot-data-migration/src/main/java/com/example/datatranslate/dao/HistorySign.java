package com.example.datatranslate.dao;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("sign_in")
public class HistorySign {
    private String userId;

    private String guildId;

    private Integer days;
}
