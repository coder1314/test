package com.example.datatranslate.pojo.request;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class SignQuery {
    private String guildId;

    private String userId;

    // else params
}
