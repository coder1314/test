package com.example.datatranslate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.datatranslate.dao.Socialite;
import com.example.datatranslate.dao.mapper.SocialiteMapper;
import com.example.datatranslate.service.SocialiteService;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class SocialiteServiceImpl extends ServiceImpl<SocialiteMapper, Socialite> implements SocialiteService {
    @Override
    public void insertAll(List<Socialite> socialites) {
        List<Socialite> edata = this.lambdaQuery().list();
        Map<String, Integer> map = new HashMap<>();
        edata.forEach(e -> {
            if(!map.containsKey(e.getSocialId())){
                map.put(e.getSocialId(), 0);
            }
        });
        edata.clear();
        socialites.forEach(e -> {
            if(!map.containsKey(e.getSocialId())){
                edata.add(e);
            }
        });
        log.info("socialite start");
        this.saveBatch(edata);
        log.info("soclialite end");
    }
}
