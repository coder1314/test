package com.example.datatranslate.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.datatranslate.dao.SignInfo;

import java.util.List;

public interface SignService extends IService<SignInfo> {
    // 获取fanbook_sign的所有记录
    List<SignInfo> getList(String guildId);
}
