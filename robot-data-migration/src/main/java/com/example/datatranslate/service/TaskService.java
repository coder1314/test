package com.example.datatranslate.service;

/**
* @Project: service
* @description: TaskService.java
* @date: Tuesday, 3rd August 2021 10:13:58 am
* @author: crisen (crisen@crisen.org)
* @Copyright 2021 - 2021 forfree, idreamsky.inc
*/


import com.example.datatranslate.dao.*;
import com.example.datatranslate.pojo.request.SignQuery;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
@AllArgsConstructor
public class TaskService  implements Runnable {
    @Resource
    private SignService signService;
    @Resource
    private SystemUserService systemUserService;
    @Resource
    private SocialiteService socialiteService;
    @Resource
    private UserService userService;
    @Resource
    private RecordService recordService;
    @Resource
    private SignInfoService signInfoService;
    @Resource
    private HistorySignService historySignService;

    @Resource
    private SignQuery query;

    public void setQuery(SignQuery query){
        this.query = query;
    }

    public void run() {
        fbtInfSystemUsers();
        fbtInfSocialites();
        fbtInfUsers();
        fbtInfRecords();
        fbtInfSignInfo();
    }

    public void fbtInfSystemUsers(){
        List<SignInfo> signInfos = signService.getList(query.getGuildId());
        Map<Integer, List<SignInfo>> groups = sortAndGroupByUsername(signInfos);
        List<SystemUser> systemUsers = getSystemUsers(groups);
        log.info("init system_user");
        systemUserService.insertAll(systemUsers);
    }

    public void fbtInfSocialites(){
        List<SystemUser> systemUsers = systemUserService.getAll();


        List<Socialite> socialites = getSocialites(systemUsers);
        log.info("init socialite user");
        socialiteService.insertAll(socialites);
    }

    public void fbtInfUsers(){
        List<SystemUser> all = systemUserService.getAll();
        List<User> users = getUsers(all);
        log.info("init user");
        userService.insertAll(users);
    }

    public void fbtInfRecords(){
        List<SystemUser> systemUsers = systemUserService.getAll();
        List<User> users = userService.getAll();
        List<SignInfo> signInfos = signService.getList(query.getGuildId());
        List<Record> records = getRecords(users, systemUsers, signInfos);
//        records.forEach(System.out::println);
        log.info("init records");
        recordService.insertAll(records);
    }

    public void fbtInfSignInfo(){
        List<SignInfo> signInfos = signService.getList(query.getGuildId());
        Map<Integer, List<SignInfo>> groups = sortAndGroupByUsername(signInfos);
        List<Sign> signs = getSigns(groups);
        log.info("");
        signInfoService.insertAll(signs);
    }

    private Map<Integer, List<SignInfo>> sortAndGroupByUsername(List<SignInfo> signInfos){
        signInfos.sort((a, b) -> a.getUsername() - b.getUsername());

        Map<Integer, List<SignInfo>> single = new TreeMap<>();
        signInfos.forEach(e -> {
            if(!single.containsKey(e.getUsername())){
                List<SignInfo> list = new ArrayList<>();
                list.add(e);
                single.put(e.getUsername(), list);
            }else{
                single.get(e.getUsername()).add(e);
            }
        });
        return single;
    }

    private List<SystemUser> getSystemUsers(Map<Integer, List<SignInfo>> groups){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return groups.values().stream()
                .map(signInfos -> {
                    // 获取最新的昵称
                    SignInfo signInfo = signInfos.get(signInfos.size() - 1);
                    try {
                        return SystemUser.builder()
                                .username(signInfo.getUid() + "-" + signInfo.getUsername())
                                .name(signInfo.getNickName())
                                .nickname(signInfo.getNickName())
                                .createdAt(format.parse(format.format(new Date())))
                                .updatedAt(format.parse(format.format(new Date())))
                                .avatar("")
                                .build();
                    } catch (ParseException parseException) {
                        parseException.printStackTrace();
                    }
                    return null;
                })
                .collect(Collectors.toList());
    }

    private List<Socialite> getSocialites(List<SystemUser> systemUsers){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        // 使用新产生的system_users表来生成，避免多余的查询的遍历操作
        return systemUsers.stream()
                .map(e -> {
                    try {
                        return Socialite.builder()
                                .socialId(e.getUsername().split("-")[0])
                                .socialType("fanbook")
                                .name(e.getUsername().split("-")[1])
                                .nickname(e.getNickname())
                                .systemId(e.getId())
                                .avatar("")
                                .createdAt(format.parse(format.format(new Date())))
                                .updatedAt(format.parse(format.format(new Date())))
                                .build();
                    } catch (ParseException parseException) {
                        parseException.printStackTrace();
                    }
                    return null;
                })
                .collect(Collectors.toList());
    }

    private List<User> getUsers(List<SystemUser> systemUsers){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return systemUsers.stream()
                .map(e -> {
                    try {
                        return User.builder()
                                .appId(999)
                                .nickname(e.getNickname())
                                .systemId(e.getId())
                                .tenantId("idreamsky")
                                .createdAt(format.parse(format.format(new Date())))
                                .updatedAt(format.parse(format.format(new Date())))
                                .password("123456")
                                .build();
                    } catch (ParseException parseException) {
                        parseException.printStackTrace();
                    }
                    return null;
                })
                .collect(Collectors.toList());
    }

    private List<Record> getRecords(List<User> users, List<SystemUser> systemUsers, List<SignInfo> signInfos){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Map<Integer, Integer> usersMap = getUsersMap(users);
        Map<String, Integer> systemUsersMap = getSystemUsersMap(systemUsers);
        Date date = new Date();
        return signInfos.stream()
                .map(e -> {
                    date.setTime(e.getCreated() * 1000L);
                    try {
                        Integer systemId = systemUsersMap.get(e.getUid());
                        Integer userId = usersMap.get(systemId);
                        return Record.builder()
                                .appId(999)
                                .systemId(systemId)
                                .userId(userId)
                                .signDate(e.getSignDay())
                                .signAt(format.parse(format.format(date)))
                                .createdAt(format.parse(format.format(new Date())))
                                .updatedAt(format.parse(format.format(new Date())))
                                .build();
                    } catch (ParseException parseException) {
                        parseException.printStackTrace();
                    }
                    return null;
                })
                .collect(Collectors.toList());
    }

    private Map<Integer, Integer> getUsersMap(List<User> users){
        Map<Integer, Integer> userMap = new HashMap<>();
        users.forEach( e -> {
            userMap.put(e.getSystemId(), e.getId());
        });
        return userMap;
    }

    private Map<String, Integer> getSystemUsersMap(List<SystemUser> systemUsers){
        Map<String, Integer> systemUserMap = new HashMap<>();
        systemUsers.forEach(e -> {
            systemUserMap.put(e.getUsername().split("-")[0], e.getId());
        });
        return systemUserMap;
    }

    private List<Sign> getSigns(Map<Integer, List<SignInfo>> groups){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Map<String, Integer> map = new HashMap<>();
        historySignService.getAll(query.getGuildId()).stream().forEach(e -> {
            map.put(e.getUserId(), e.getDays());
        });
        Map<String, Integer> systemUserMap = new HashMap<>();
        systemUserService.getAll().stream().forEach(e -> {
            systemUserMap.put(e.getUsername().split("-")[0], e.getId());
        });
        Map<Integer, Integer> userMap = new HashMap<>();
        userService.getAll().stream().forEach(e -> {
            userMap.put(e.getSystemId(), e.getId());
        });
        return groups.values().stream()
                .map(days -> {
                    Map<String, Integer> signDays = getSignDays(days);
                    String uid = days.get(0).getUid();
                    try {
                        return Sign.builder()
                                .appId(9999)
//                                .historySignDays(signDays.get("sum"))
                                .historySignDays(map.get(uid))
                                .userId(userMap.get(systemUserMap.get(uid)))
                                .signDays(signDays.get("sum"))
                                .lastSignDate(days.get(days.size() - 1).getSignDay())
                                .maxContinuingDays(signDays.get("max"))
                                .continuingDays(signDays.get("continuing"))
                                .createdAt(format.parse(format.format(new Date())))
                                .updatedAt(format.parse(format.format(new Date())))
                                .build();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    return null;
                })
                .collect(Collectors.toList());
    }

    private Map<String, Integer> getSignDays(List<SignInfo> days){
        int count = 1, continuing = 1, max = 1;
        for (int i = 1; i < days.size(); i++) {
            Date one = days.get(i).getSignDay();
            Date two = days.get(i - 1).getSignDay();
            long d = (one.getTime() - two.getTime())/86400000;
            if(d == 1) continuing++;
            else{
                max = Math.max(max, continuing);
                continuing = 1;
            }
            count++;
        }
        max = Math.max(max, continuing);
        Map<String, Integer> signDays = new HashMap<>();
        signDays.put("sum", count);
        signDays.put("continuing", continuing);
        signDays.put("max", max);
        return signDays;
    }
}
