package com.example.datatranslate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.datatranslate.dao.Record;
import com.example.datatranslate.dao.mapper.RecordMapper;
import com.example.datatranslate.service.RecordService;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RecordServiceImpl extends ServiceImpl<RecordMapper, Record> implements RecordService {
    @Override
    public void insertAll(List<Record> records) {
        // 依据 sign时间 和 userId 来做差集
        List<Record> edata = this.lambdaQuery().list();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm::ss");
        Map<String, Integer> map = new HashMap<>();
        edata.forEach(e -> {
            String date = format.format(e.getSignAt());
            if(!map.containsKey(date)){
                map.put(date, e.getUserId());
            }
        });
        edata.clear();
        records.forEach(e -> {
            String date = format.format(e.getSignAt());
            if(!map.containsKey(date)){
                Integer id = map.get(date);
                if(id == null) edata.add(e);
                else if(id != e.getUserId()) edata.add(e);
            }
        });
        this.saveBatch(edata);
    }
}
