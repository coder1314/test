package com.example.datatranslate.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.datatranslate.dao.SystemUser;

import java.util.List;

public interface SystemUserService extends IService<SystemUser> {
    void insertAll(List<SystemUser> users);

    List<SystemUser> getAll();

    SystemUser findByUsername(String username);
}
