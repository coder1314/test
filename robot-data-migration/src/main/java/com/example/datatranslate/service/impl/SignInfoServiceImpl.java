package com.example.datatranslate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.datatranslate.dao.Sign;
import com.example.datatranslate.dao.mapper.SignMapper;
import com.example.datatranslate.service.SignInfoService;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class SignInfoServiceImpl extends ServiceImpl<SignMapper, Sign> implements SignInfoService {
    @Override
    public void insertAll(List<Sign> signs) {
        // 存在根据id更新，不存在直接插入 (更新被禁用中)
        List<Sign> edata = this.lambdaQuery().list();
        Map<Integer, Integer> map = new HashMap<>();
        edata.forEach(e -> {
            if(!map.containsKey(e.getUserId())){
                map.put(e.getUserId(), 0);
            }
        });
        edata.clear();
        List<Sign> list = new ArrayList<>();
        signs.forEach(e -> {
            if(!map.containsKey(e.getUserId())){
                edata.add(e);
            }
            // else{
            //     list.add(e);
            // }
        });
        log.info("signInfo start");
        insert(edata);
        update(list);
        log.info("signInfo end");
    }

    private void insert(List<Sign> signs){
        this.saveBatch(signs);
    }

    private void update(List<Sign> signs){
        this.updateBatchById(signs, signs.size());
    }
}
