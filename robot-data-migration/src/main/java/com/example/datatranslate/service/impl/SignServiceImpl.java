package com.example.datatranslate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.datatranslate.dao.SignInfo;
import com.example.datatranslate.dao.mapper.SignInfoMapper;
import com.example.datatranslate.service.SignService;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class SignServiceImpl extends ServiceImpl<SignInfoMapper, SignInfo> implements SignService {
    @Override
    public List<SignInfo> getList(String guildId) {
        if(guildId == null) return this.lambdaQuery().list();
        return this.lambdaQuery().eq(SignInfo::getGuildId, guildId).list();
    }
}
