package com.example.datatranslate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.datatranslate.dao.User;
import com.example.datatranslate.dao.mapper.UserMapper;
import com.example.datatranslate.service.UserService;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Override
    public void insertAll(List<User> users) {
        List<User> list = this.lambdaQuery().list();
        HashMap<Integer, Integer> userMap =  new HashMap<>();
        list.forEach(e->{
            userMap.put(e.getSystemId(), e.getId());
        });
        List<User> insertUsers = new LinkedList<>();
        users.forEach( e -> {
            if(!userMap.containsKey( e.getSystemId())){
                insertUsers.add(e);
            }
        });
        log.info("user start");
        this.saveBatch(insertUsers);
        log.info("user end");
    }

    @Override
    public User findBySystemId(Integer id) {
        List<User> list = this.lambdaQuery().eq(User::getSystemId, id).list();
//        list.forEach(System.out::println);
        return list.get(0);
    }

    @Override
    public List<User> getAll() {
        return this.lambdaQuery().list();
    }
}
