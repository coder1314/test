package com.example.datatranslate.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.datatranslate.dao.Socialite;

import java.util.List;

public interface SocialiteService extends IService<Socialite> {
    void insertAll(List<Socialite> socialites);
}
