package com.example.datatranslate.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.datatranslate.dao.Sign;

import java.util.List;

public interface SignInfoService extends IService<Sign> {
    void insertAll(List<Sign> signs);
}
