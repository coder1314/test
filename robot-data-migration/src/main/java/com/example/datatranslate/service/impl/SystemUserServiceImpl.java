package com.example.datatranslate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.datatranslate.dao.SystemUser;
import com.example.datatranslate.dao.mapper.SystemUserMapper;
import com.example.datatranslate.service.SystemUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

@Service
@Slf4j
public class SystemUserServiceImpl extends ServiceImpl<SystemUserMapper, SystemUser> implements SystemUserService {

    @Override
    public void insertAll(List<SystemUser> users) {
        // 获取已存在的数据列表
        List<SystemUser> list = getAll();
        HashMap<String, Integer> userMap =  new HashMap<>();
        list.forEach(e->{
            userMap.put(e.getUsername(), e.getId());
        });
        List<SystemUser> insertUsers = new LinkedList<>();
        users.forEach( e -> {
            if(!userMap.containsKey( e.getUsername() )){
                insertUsers.add(e);
            }
        });
        log.info("system user start");
        this.saveBatch(insertUsers);
        log.info("system user end");
    }

    @Override
    public List<SystemUser> getAll() {
        return this.lambdaQuery().list();
    }

    @Override
    public SystemUser findByUsername(String username) {
        List<SystemUser> list = this.lambdaQuery().eq(SystemUser::getUsername, username).list();
//        list.forEach(System.out::println);
        return list.get(0);
    }
}
