package com.example.datatranslate.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.datatranslate.dao.Record;

import java.util.List;

public interface RecordService extends IService<Record> {
    void insertAll(List<Record> records);
}
