package com.example.datatranslate.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.datatranslate.dao.HistorySign;

public interface HistorySignService extends IService<HistorySign> {
    Integer findDaysByUserId(String id);

    List<HistorySign> getAll(String guildId);
}
