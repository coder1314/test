package com.example.datatranslate.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.datatranslate.dao.User;

import java.util.List;

public interface UserService extends IService<User> {
    void insertAll(List<User> users);

    User findBySystemId(Integer id);

    List<User> getAll();
}
