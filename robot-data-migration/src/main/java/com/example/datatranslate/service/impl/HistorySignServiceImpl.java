package com.example.datatranslate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.datatranslate.dao.HistorySign;
import com.example.datatranslate.dao.mapper.HistorySignMapper;
import com.example.datatranslate.service.HistorySignService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class HistorySignServiceImpl extends ServiceImpl<HistorySignMapper, HistorySign> implements HistorySignService {
    @Override
    public Integer findDaysByUserId(String id) {
        List<HistorySign> list = this.lambdaQuery().eq(HistorySign::getUserId, id).list();
        if (CollectionUtils.isEmpty(list)) return 0;
        return list.get(0).getDays();
    }

    @Override
    public List<HistorySign> getAll(String guildId) {
        if(guildId == null) return this.lambdaQuery().list();
        return this.lambdaQuery().eq(HistorySign::getGuildId, guildId).list();
    }
}
