package com.example.datatranslate;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.example.datatranslate.dao")
public class DataTranslateApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataTranslateApplication.class, args);
    }

}
